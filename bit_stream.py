def encode(bits: str) -> bytes:
    bits += '1'
    bits += '0' * ((8 - (len(bits) % 8)) % 8)
    return int(bits, 2).to_bytes(len(bits) // 8, 'big')


def decode(b: bytes) -> str:
    result = f'{int.from_bytes(b, "big"):0{len(b) * 8}b}'
    return result.rsplit('1', maxsplit=1)[0]
