from base64 import urlsafe_b64encode as b64encode, urlsafe_b64decode as b64decode

import bit_stream
import complex_stream
import primitive_stream
from complex_stream import Dropdown
from primitive_stream import Flag, Int8

tourney_settings = {
    'KEYS_SETTING': ['KEYS_VANILLA', 'KEYS_STANDARD', 'KEYS_OUTSIDE', 'KEASY', 'KEYSANITY'],
    'HUNDO': 'bool',
    'DUNGEON': ['NONE', 'BARREN', 'BARRENSUR', 'SHUFFLE_ELEMENTS'],
    'GLITCHLESS': 'bool',
    'WHYWOULDYOUDOTHISTOYOURSELF': 'bool',
    'RUPEEMANIA': 'bool',
    'WEAPONS': 'bool',
    'TRAPS': 'bool',
    'TIMEDOHKO': 'bool',
    'MUSIC_RANDO': 'bool',
    'FIREROD': 'bool',
    'AD_PED': 'bool',
    'AD_PED_NUM': '8bit',
    'ITEM_POOL': ['ITEM_POOL_NORMAL', 'ITEM_POOL_RIP', 'ITEM_POOL_PLENTIFUL'],
    'FUSION_SETTING': ['NO_FUSIONS', 'OPENFUSIONS', 'OPENWORLD'],
    'FUSION_SKIPS': ['ALLOW_FUSION_SKIPS', 'INSTANT_SKIP', 'NO_FUSION_SKIP'],
    'DHC_SETTING': ['NORMALDHC', 'NODHC', 'OPENDHC', 'PEDITEMS'],
    'FIGURINE_HUNT': 'bool',
    'FIGURINE_COUNT': '8bit',
    'FIGURINE_EXTRA': '8bit',
    'SWORD_SETTING': ['0SWORD', '1', '2', '3', '4', '5'],
    'ELEMENT_SETTING': ['4', '3', '2', '1', '0ELEMENT'],
}


def compare(data1, data2):
    for key in data1:
        if key in data2:
            assert data1[key] == data2[key]


def main():
    data = {
        'KEYS_SETTING': Dropdown('KEASY'),
        'HUNDO': Flag(False),
        'DUNGEON': Dropdown('NONE'),
        'GLITCHLESS': Flag(True),
        'WHYWOULDYOUDOTHISTOYOURSELF': Flag(True),
        'RUPEEMANIA': Flag(True),
        'WEAPONS': Flag(False),
        'TRAPS': Flag(False),
        'TIMEDOHKO': Flag(False),
        'MUSIC_RANDO': Flag(False),
        'FIREROD': Flag(True),
        'AD_PED': Flag(False),
        'AD_PED_NUM': Int8(0),
        'ITEM_POOL': Dropdown('ITEM_POOL_NORMAL'),
        'FUSION_SETTING': Dropdown('OPENFUSIONS'),
        'FUSION_SKIPS': Dropdown('ALLOW_FUSION_SKIPS'),
        'DHC_SETTING': Dropdown('NORMALDHC'),
        'FIGURINE_HUNT': Flag(False),
        'FIGURINE_COUNT': Int8(0),
        'FIGURINE_EXTRA': Int8(0),
        'SWORD_SETTING': Dropdown('5'),
        'ELEMENT_SETTING': Dropdown('4'),
    }
    print(data)
    encoded = complex_stream.encode(data, tourney_settings)
    encoded = primitive_stream.encode(encoded)
    encoded = bit_stream.encode(encoded)
    encoded = 'A' + b64encode(encoded).decode()
    print(encoded)
    tourney_settings['AAAAA'] = 'flag'
    del tourney_settings['AD_PED_NUM']
    decoded = b64decode(encoded[1:].encode())
    decoded = bit_stream.decode(decoded)
    decoded = primitive_stream.decode(decoded)
    decoded = complex_stream.decode(decoded, tourney_settings)
    print(decoded)
    compare(data, decoded)


if __name__ == '__main__':
    main()
