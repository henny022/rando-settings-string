from dataclasses import dataclass
from typing import Union


@dataclass
class Flag:
    value: bool

    def encode(self):
        if self.value:
            return '01'
        return '00'

    @classmethod
    def decode(cls, stream: str):
        if stream[0] != '0':
            raise ValueError('type error, expected flag')
        return cls(stream[1] == '1'), stream[2:]


@dataclass
class Int8:
    value: int

    def encode(self):
        assert self.value < (2 ** 8)
        return f'10{self.value:08b}'

    @classmethod
    def decode(cls, stream: str):
        if stream[0:2] != '10':
            raise ValueError('type error, expected int8')
        return cls(int(stream[2:10], 2)), stream[10:]


@dataclass
class Int15:
    value: int

    def encode(self):
        assert self.value < (2 ** 15)
        return f'11{self.value:015b}'

    @classmethod
    def decode(cls, stream: str):
        if stream[0:2] != '11':
            raise ValueError('type error, expected int15')
        return cls(int(stream[2:17], 2)), stream[17:]


Primitive = Union[Flag, Int8, Int15]


def encode(data: list[tuple[int, Primitive]]):
    result = ''
    for hash, primitive in data:
        assert hash < (2 ** 8)
        result += f'{hash:08b}'
        result += primitive.encode()
    result += '1'
    result += '0' * ((8 - (len(result) % 8)) % 8)
    return result


def decode(stream: str):
    stream = stream.rsplit('1', maxsplit=1)[0]
    result = []
    while len(stream) > 0:
        hash = int(stream[:8], 2)
        if stream[8] == '0':
            value, stream = Flag.decode(stream[8:])
        elif stream[8:10] == '10':
            value, stream = Int8.decode(stream[8:])
        elif stream[8:10] == '11':
            value, stream = Int15.decode(stream[8:])
        else:
            assert False, 'unreachable'
        result.append((hash, value))
    return result
