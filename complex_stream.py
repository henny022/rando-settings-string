import typing
from dataclasses import dataclass
from typing import Union

import crc8

from primitive_stream import Primitive, Flag, Int8, Int15


def hash_key_crc8(key: str):
    hash = crc8.crc8()
    hash.update(key.encode())
    return int.from_bytes(hash.digest(), 'big')


def hash_dropdown_options(options: list[str]):
    result = []
    for option in options:
        h = hash_key_crc8(option)
        i = 0
        while h in result:
            assert i < 10
            h = hash_key_crc8(option + str(i))
            i += 1
        result.append(h)
    return result


@dataclass
class Color:
    r: int
    g: int
    b: int

    def to_primitive(self, type_info):
        v = (self.b << 10) | (self.g << 5) | self.r
        return Int15(v)

    @classmethod
    def from_primitive(cls, primitive, type_info):
        assert isinstance(primitive, Int15)
        r = primitive.value & 0x1f
        g = (primitive.value >> 5) & 0x1f
        b = (primitive.value >> 10) & 0x1f
        return cls(r, g, b)


@dataclass
class Dropdown:
    value: str

    def to_primitive(self, type_info: list[str]):
        assert self.value in type_info
        hashes = hash_dropdown_options(type_info)
        return Int8(hashes[type_info.index(self.value)])

    @classmethod
    def from_primitive(cls, primitive, type_info: list[str]):
        assert isinstance(primitive, Int8)
        hashes = hash_dropdown_options(type_info)
        assert primitive.value in hashes
        return cls(type_info[hashes.index(primitive.value)])


Complex = Union[Primitive, Color]


def encode(data: dict[str, Complex], type_info: dict[str, Union[str, list[str]]]) -> list[tuple[int, Primitive]]:
    primitives = []
    for key in sorted(type_info.keys()):
        value = data[key]
        if not isinstance(value, typing.get_args(Primitive)):
            value = value.to_primitive(type_info[key])
        entry = hash_key_crc8(key), value
        primitives.append(entry)
    return primitives


def primitive_type_match(value, type_info):
    # TODO improve this with better type info
    if isinstance(type_info, list):
        return isinstance(value, Int8)
    if type_info == 'bool':
        return isinstance(value, Flag)
    if type_info == '8bit':
        return isinstance(value, Int8)
    if type_info == '15bit':
        return isinstance(value, Int15)
    if type_info == 'color':
        return isinstance(value, Int15)


def decode(data: list[tuple[int, Primitive]], type_info: dict[str, Union[str, list[str]]]) -> dict[str, Complex]:
    result = {}
    keys = list(sorted(type_info.keys()))
    key_hashes = [hash_key_crc8(key) for key in keys]
    next = 0
    for hash, value in data:
        i = next
        if hash not in key_hashes[i:]:
            # setting got removed
            continue
        # skip added settings
        while key_hashes[i] != hash:
            i += 1
        key = keys[i]
        if not primitive_type_match(value, type_info[key]):
            # probably setting got removed
            continue
        if isinstance(type_info[key], list):
            value = Dropdown.from_primitive(value, type_info[key])
        if type_info[key] == 'color':
            value = Color.from_primitive(value, type_info[key])
        result[key] = value
        next = i + 1
    for key in keys:
        if key not in result:
            # rando implementations would probably use default value here
            result[key] = None
    return result
